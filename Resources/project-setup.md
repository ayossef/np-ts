# Main Project Setup commands
## Create Node Project with typescript support
- Init a new node Project
``` bash
npm init -y
```
- Add typescript support
``` bash
npm i -D typescript
```
- Init Typescript Compiler (TSC) confirguration file
``` bash
npx tsc init
```
- Add Jest unit testing support
``` bash
npm i -D jest ts-jest @types/jest
```
- Tell jest to consider typescript as preprocessor
``` bash
npx ts-jest config:init
```
- Add Testing to package.json 
``` json
"scripts": {
    ...
    "test": "jest"
  },
```
