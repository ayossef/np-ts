# Prepare Development Environment - Ubuntu

- Update packages
``` bash
sudo apt update
```

- Install Node from Binaries
``` bash
curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash - &&\
sudo apt-get install -y nodejs
```

- Verify Installation - node v.18.x
```
node --version
npm --version
```

